class CreateSalesGroups < ActiveRecord::Migration
  def change
    create_table :sales_groups do |t|
    	t.string :sales_group_id
    	t.string :sales_goup_name
    	t.string :sales_group_manager
    	t.integer :code
      t.timestamps
    end
  end
end
