class CreateEmployees < ActiveRecord::Migration
  def change
    create_table :employees do |t|
    	t.string :employee_id
    	t.string :position_id
    	t.string :email
    	t.string :fullname
    	t.string :address
    	t.string :birthdate
    	t.string :joindate
    	t.integer :phone
    	t.integer :mobile
    	t.string :employee_type_id
    	t.string :sales_group_id
      t.timestamps
    end
  end
end
