class CreateEmployeeTypes < ActiveRecord::Migration
  def change
    create_table :employee_types do |t|
    	t.string :employee_type_id
    	t.integer :code
    	t.string :employee_type_name
      t.timestamps
    end
  end
end
