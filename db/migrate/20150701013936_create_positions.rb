class CreatePositions < ActiveRecord::Migration
  def change
    create_table :positions do |t|
    	t.string :position_id
    	t.string :description
    	t.integer :code 
      t.timestamps
    end
  end
end
