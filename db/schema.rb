# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20150701014134) do

  create_table "employee_types", force: true do |t|
    t.string   "employee_type_id"
    t.integer  "code"
    t.string   "employee_type_name"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "employees", force: true do |t|
    t.string   "employee_id"
    t.string   "position_id"
    t.string   "email"
    t.string   "fullname"
    t.string   "address"
    t.string   "birthdate"
    t.string   "joindate"
    t.integer  "phone"
    t.integer  "mobile"
    t.string   "employee_type_id"
    t.string   "sales_group_id"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "positions", force: true do |t|
    t.string   "position_id"
    t.string   "description"
    t.integer  "code"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "sales_groups", force: true do |t|
    t.string   "sales_group_id"
    t.string   "sales_goup_name"
    t.string   "sales_group_manager"
    t.integer  "code"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "students", force: true do |t|
    t.string   "name"
    t.string   "addres"
    t.integer  "age"
    t.integer  "no_telp"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "users", force: true do |t|
    t.string   "username"
    t.string   "email"
    t.string   "password"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

end
