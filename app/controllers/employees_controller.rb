class EmployeesController < ApplicationController
	def index
		@employees = Employee.all
	end
	def new
		@employee = Employee.new 
	end
	def create
		@employee = Employee.new(employee_params)
		if @employee.save
			redirect_to employees_path
		else
			redirect_to new_employee_path
		end
	end
	def edit
		@employee = Employee.find(params[:id])
	end
	def update
		@employee = Employee.find(params[:id])
		if @employee.update(employee_params)
		redirect_to employees_path
	else
		redirect_to new_employee_part
	end
end
	def destroy
		@employee = Employee.find(params[:id])
		if @employee.destroy
		redirect_to employees_path
	else
		redirect_to new_employee_path
	end
end
private 
 	def employee_params
		params.require(:employee).permit(:employee_id , :position_id,  :email, :fullname, :address, :birthdate, :joindate, :phone, :mobile, :employee_type_id, :sales_group_id)
	end
end
