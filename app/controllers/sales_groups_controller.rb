class SalesGroupsController < ApplicationController
	def index
		@sales_groups = SalesGroup.all
	end
	def new
		@sales_group = SalesGroup.new 
	end
	def create
		@sales_group = SalesGroup.new(sales_group_params)
		if @sales_group.save
			redirect_to sales_groups_path
		else
			redirect_to new_sales_group_path
		end
	end
	def edit
		@sales_group = SalesGroup.find(params[:id])
	end
	def update
		@sales_group = SalesGroup.find(params[:id])
		if @sales_group.update(sales_group_params)
		redirect_to sales_groups_path
	else
		redirect_to new_sales_group_part
	end
end
	def destroy
		@sales_group = SalesGroup.find(params[:id])
		if @sales_group.destroy
		redirect_to sales_groups_path
	else
		redirect_to new_sales_group_path
	end
end
private 
 	def sales_group_params
		params.require(:sales_group).permit(:sales_group_id , :sales_group_name, :sales_group_manager, :code)
	end
end
