class UsersController < ApplicationController
  def index
  	@users = User.all
  end

  def new
  	@user = User.new
  end

  
  def create

        @user = User.new(params_user)

        if @user.save

            flash[:notice] = "Success Add Records"

            redirect_to root_url

        else

            flash[:error] = "data not valid"

            render "new"

        end

    end
def edit
		@user = User.find(params[:id])
	end
	def update
		@user = User.find(params[:id])
		if @user.update(user_params)
		redirect_to users_path
	else
		redirect_to new_user_part
	end
end
	def destroy
		@user = User.find(params[:id])
		if @user.destroy
		redirect_to users_path
	else
		redirect_to new_user_path
	end
end
    
    private

        def params_user

            params.require(:user).permit(:username, :email, :password, :password_confirmation)

        end
end
