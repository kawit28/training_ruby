class EmployeeTypesController < ApplicationController
	def index
		@employee_types = EmployeeTypes.all
	end
	def new
		@employee_type = EmployeeTypes.new 
	end
	def create
		@employee_type = EmployeeTypes.new(employee_type_params)
		if @employee_type.save
			redirect_to employee_types_path
		else
			redirect_to new_employee_type_path
		end
	end
	def edit
		@employee_type = EmployeeTypes.find(params[:id])
	end
	def update
		@employee_type = EmployeeTypes.find(params[:id])
		if @employee_type.update(employee_type_params)
		redirect_to employee_types_path
	else
		redirect_to new_employee_type_part
	end
end
	def destroy
		@employee_type = EmployeeTypes.find(params[:id])
		if @employee_type.destroy
		redirect_to employee_types_path
	else
		redirect_to new_employee_type_path
	end
end
private 
 	def employee_type_params
		params.require(:employee_type).permit(:employee_type_id , :employee_type_name, :code)
	end
end
